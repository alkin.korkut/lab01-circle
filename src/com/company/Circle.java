package com.company;

public class Circle {
    public double x;
    public double y;
    public double r;

    public double getX(){
        return this.x;
    }

    public void setX(double x){
        this.x = x;
    }

    public double getY(){
        return this.y;
    }

    public void setY(double y){
        this.y = y;
    }

    public double getR(){
        return this.r;
    }

    public void setR(double r){
        this.r=r;
    }

    public double getArea(){
        return Math.PI*Math.pow(r,2);
    }

    public void translate(double dx, double dy){
        this.x+=dx;
        this.y+=dy;
    }

    public double scale(double scaler){
        this.r*=scaler;
        return this.r;
    }


}
