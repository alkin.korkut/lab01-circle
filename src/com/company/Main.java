package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Circle circle = new Circle();

        System.out.println("State 1: ");
        circle.setX(-2.06);
        circle.setY(-0.23);
        circle.setR(1.33);
        System.out.println("X:"+circle.getX()+" Y:"+circle.getY()+" R:"+circle.getR());

        System.out.println("State 2: ");
        circle.translate(2.06,5.23);
        circle.scale(1.88/1.33);
        System.out.println("X:"+circle.getX()+" Y:"+circle.getY()+" R:"+circle.getR());

        System.out.println("State 3: ");
        circle.translate(5.0,0);
        circle.scale(1);
        System.out.println("X:"+circle.getX()+" Y:"+circle.getY()+" R:"+circle.getR());

        System.out.println("State 4: ");
        circle.scale(0.94/1.88);
        System.out.println("X:"+circle.getX()+" Y:"+circle.getY()+" R:"+circle.getR());

        System.out.println("State 5: ");
        circle.translate(-1.0,-1.0);
        circle.scale(3.1/0.94);
        System.out.println("X:"+circle.getX()+" Y:"+circle.getY()+" R:"+circle.getR());


    }
}
